﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Audio;
using System.Collections.Generic;

public class Player : NetworkBehaviour
{
    public float Acceleration = 1f;
    public float MovementSpeed = 10f;
    public float RotationSpeed = 1f;
    public float CollisionDamage = 1;
    public float MaxLife = 50;
    public GameObject ProjectilePrefab;
    public AudioMixer MusicMixer;
    public static GameObject LocalPlayerGameObject;
    public static GameObject LocalPlayerCameraGameObject;
    public static NetworkConnection LocalPlayerNetworkConnection;

    private Transform myTransform;
    private Transform myCanon;
    private Rigidbody myRigidbody;
    private RectTransform myCanvasTransform;
    private RectTransform myLifeBarTransform;
    private AudioSource myShootAudioSource;
    private SphereCollider myTriggerCollider;
    private List<Transform> EnemiesInRange;

    [SyncVar]
    private float Life;

    void Awake()
    {
        if (NetworkServer.connections.Count > 0)
        {
            LocalPlayerNetworkConnection = NetworkServer.connections[NetworkServer.connections.Count - 1];
        }
    }

	void Start()
    {
        myTransform = transform;
        myCanon = myTransform.Find("Canon");
        myRigidbody = myTransform.GetComponent<Rigidbody>();
        myCanvasTransform = myTransform.Find("Canvas").GetComponent<RectTransform>();
        myLifeBarTransform = myCanvasTransform.GetChild(0).GetComponent<RectTransform>();
        myShootAudioSource = myTransform.GetComponent<AudioSource>();

        if (!isLocalPlayer)
        {
            gameObject.GetComponentInChildren<Camera>().enabled = false;
            gameObject.GetComponentInChildren<AudioListener>().enabled = false;
        }
        else
        {
            LocalPlayerGameObject = gameObject;
            LocalPlayerCameraGameObject = LocalPlayerGameObject.transform.Find("Camera").gameObject;
            myCanvasTransform.gameObject.SetActive(false);

            myCanvasTransform = GameObject.Find("HUD").GetComponent<RectTransform>();
            myLifeBarTransform = myCanvasTransform.GetChild(0).GetComponent<RectTransform>();
            EnemiesInRange = new List<Transform>();
            myTriggerCollider = myTransform.GetComponent<SphereCollider>();
        }

        Life = MaxLife;
	}
	
	void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            Controls();
            MusicManager();
        }
        else
        {
            myCanvasTransform.LookAt(LocalPlayerCameraGameObject.transform);
        }

        LifeManager();
    }

    void Controls()
    {
        myTransform.Rotate(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);

        if (Input.GetKey(KeyCode.W))
        {
            myRigidbody.AddRelativeForce(0, 0, Acceleration);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            myRigidbody.AddRelativeForce(0, 0, -Acceleration);
        }

        if (Input.GetKey(KeyCode.A))
        {
            myRigidbody.AddRelativeForce(-Acceleration, 0, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            myRigidbody.AddRelativeForce(Acceleration, 0, 0);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            myRigidbody.AddRelativeForce(0, Acceleration, 0);
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            myRigidbody.AddRelativeForce(0, -Acceleration, 0);
        }

        if (myRigidbody.velocity.magnitude > MovementSpeed)
        {
            myRigidbody.velocity = myRigidbody.velocity.normalized * MovementSpeed;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            myTransform.Rotate(0, 0, RotationSpeed);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            myTransform.Rotate(0, 0, -RotationSpeed);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            myShootAudioSource.Play();
            CmdShoot();
        }
    }

    void LifeManager()
    {
        float tScalingFactor = Life / MaxLife;
        tScalingFactor = (tScalingFactor < 0.0f) ? 0 : tScalingFactor;
        myLifeBarTransform.localScale = new Vector3(tScalingFactor, 1, 1);

        if (Life <= 0)
        {
            if (LocalPlayerNetworkConnection != null)
            {
                LocalPlayerNetworkConnection.Disconnect();
            }
            else
            {
                NetworkServer.Shutdown();
            }
        }
    }

    void MusicManager()
    {
        //Debug.Log(Time.time + ": " + EnemiesInRange.Count);

        if (EnemiesInRange.Count > 0)
        {
            GameManager.myGameManager.ChangeMusic(true);
            EnemiesInRange.RemoveAll(t => t == null);
            EnemiesInRange.RemoveAll(t => EnemiesInRange.FindAll(t2 => Vector3.Distance(myTransform.position, t2.position) > myTriggerCollider.radius).Contains(t));
        }
        else
        {
            GameManager.myGameManager.ChangeMusic(false);
        }
    }

    public void GetDamage(float _Damage)
    {
        Life -= _Damage;
    }

    [Command]
    void CmdShoot()
    {
        GameObject tGO = (GameObject)Instantiate(ProjectilePrefab, myCanon.position, myCanon.rotation);
        NetworkServer.Spawn(tGO);
    }

    void OnCollisionEnter(Collision _col)
    {
        Life -= CollisionDamage;
    }

    void OnTriggerEnter(Collider _col)
    {
        if (isLocalPlayer)
        {
            if (_col.tag == "Player")
            {
                EnemiesInRange.Add(_col.transform);
                //Debug.Log(Time.time + "Enemy added!");
            }
        }
    }

    void OnTriggerExit(Collider _col)
    {
        if (isLocalPlayer)
        {
            if (_col.tag == "Player")
            {
                EnemiesInRange.Remove(_col.transform);
                //Debug.Log(Time.time + "Enemy removed!");
            }
        }
    }
}
