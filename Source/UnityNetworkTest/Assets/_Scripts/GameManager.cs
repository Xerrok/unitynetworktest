﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public static GameManager myGameManager;

    public float MusicTransitionTime = 0.75f;
    public GameObject MainCamera;
    public Transform ManagerHolder;
    public AudioMixerSnapshot OutOfBattle, InBattle;

	void Start()
    {
        myGameManager = this;
        MainCamera = Camera.main.gameObject;
        MainCamera.SetActive(false);

        ManagerHolder = transform.parent;
        GameObject.Find("NetworkManager").transform.parent = ManagerHolder;
        GameObject.Find("MusicManager").transform.parent = ManagerHolder;
    }
	
	void Update()
    {
	    
	}

    public void ChangeMusic(bool _InBattle)
    {
        if(_InBattle)
        {
            InBattle.TransitionTo(MusicTransitionTime);
        }
        else
        {
            OutOfBattle.TransitionTo(MusicTransitionTime);
        }
    }

    void OnDisconnectedFromServer(NetworkDisconnection _info)
    {
        MainCamera.SetActive(true);
    }
}
