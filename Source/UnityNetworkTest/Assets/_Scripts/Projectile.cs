﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Projectile : NetworkBehaviour
{
    public float Damage = 5;
    public float MaxDistance = 1000f;

    Transform myTransform;
    Rigidbody myRigidbody;
    Vector3 StartPos;

	void Start()
    {
        myTransform = transform;
        myRigidbody = myTransform.GetComponent<Rigidbody>();
        StartPos = myTransform.position;

        myRigidbody.AddRelativeForce(0, 0, 100, ForceMode.Impulse);
	}

    void FixedUpdate()
    {
        if(Vector3.Distance(myTransform.position, StartPos) >= MaxDistance)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider _col)
    {
        if (_col.tag != "Player")
        {
            Destroy(_col.gameObject);
            Destroy(gameObject);
        }
        else if(!_col.isTrigger)
        {
            _col.gameObject.GetComponent<Player>().GetDamage(Damage);
            Destroy(gameObject);
        }
    }
}
