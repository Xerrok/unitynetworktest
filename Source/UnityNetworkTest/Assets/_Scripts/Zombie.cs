﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour
{
    Transform myTransform;
    Animator myAnimator;
    bool IsWalking = false;
    float WalkingTimer;
    float MinWalkTime = 3;
    float MaxWalkTime = 9;
    float MinWaitTime = 0.25f;
    float MaxWaitTime = 3;
    float WalkSpeed = 10;

	void Start()
    {
        myTransform = transform;
        myAnimator = myTransform.GetComponent<Animator>();
        WalkingTimer = 3;
	}

	void Update()
    {
	    if(WalkingTimer > 0)
        {
            WalkingTimer -= Time.deltaTime;
        }
        else
        {
            IsWalking = !IsWalking;
            myAnimator.SetBool("IsWalking", IsWalking);
            if(IsWalking)
            {
                myTransform.Rotate(0, Random.Range(0.0f, 360.0f), 0);
                WalkingTimer = Random.Range(MinWalkTime, MaxWalkTime);
            }
            else
            {
                WalkingTimer = Random.Range(MinWaitTime, MaxWaitTime);
            }
        }

        if(IsWalking)
        {
            myTransform.RotateAround(myTransform.parent.position, myTransform.right, WalkSpeed * Time.deltaTime);
        }
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(myTransform.parent.position, 0.1f);
    }
}
